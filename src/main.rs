extern crate xdg_basedir;
extern crate inotify;
extern crate relink;

use inotify::INotify;
use inotify::ffi::*;

use std::fs;

fn main() {
    let config_dir = xdg_basedir::dirs::get_config_home().unwrap();
    let relink_dir = config_dir.join("relink");

    if !relink_dir.exists() {
        println!("Creating <config>/relink");
        fs::create_dir(&relink_dir).unwrap();
    }

    let in_dir = relink_dir.join("in");
    let out_dir = relink_dir.join("out");

    if !in_dir.exists() {
        panic!("<config>/relink/in does not exist");
    }
    if !out_dir.exists() {
        panic!("<config>/relink/out does not exist");
    }

    relink::relink(&in_dir, &out_dir).unwrap();

    let mut ino = INotify::init().unwrap();
    ino.add_watch(&in_dir, IN_ALL_EVENTS).unwrap();
    loop {
        for e in ino.wait_for_events().unwrap() {
            if e.is_move_self() || e.is_delete_self() {
                panic!("Watched directory is not accessible anymore. Aborting")
            } else if e.is_modify() || e.is_move() || e.is_create() || e.is_delete() {
                relink::relink(&in_dir, &out_dir).unwrap();
                break;
            }
        }
    }
}
