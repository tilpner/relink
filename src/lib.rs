#![feature(fs_mode)]

use std::{fs, io};
use std::fs::DirEntry;
use std::path::{ Path, PathBuf };

use std::os::unix::fs::{ symlink, MetadataExt, USER_EXECUTE };

pub fn clean_dir(p: &Path) -> io::Result<()> {
    for entry in try!(fs::read_dir(p)) {
        try!(fs::remove_file(try!(entry).path()));
    }
    Ok(())
}

pub fn find_executable_entries(p: &Path) -> io::Result<Vec<PathBuf>> {
    fs::read_dir(p).map(|p|
        p.filter_map(|de| {
            if let Some(Ok(md)) = de.as_ref().ok().map(DirEntry::metadata) {
                if md.is_file() && true && (md.mode() & USER_EXECUTE != 0) {
                    return de.as_ref().ok().map(DirEntry::path);
                }
            }
            None
        }).collect())
}

pub fn relink<P>(src: P, dst: P) -> io::Result<()> where P: AsRef<Path> {
    try!(clean_dir(dst.as_ref()));

    for node in try!(fs::read_dir(src)) {
        let n = try!(node);
        let n_ft = try!(n.file_type());
        println!("{}", n.file_name().to_string_lossy());
        if n_ft.is_symlink() {
            let path = n.path();
            let _meta = try!(fs::symlink_metadata(&path));
            let executables = try!(find_executable_entries(&path));
            for e in executables {
                if let Some(file_name) = e.file_name() {
                    println!("  - {}", file_name.to_string_lossy());
                    try!(symlink(&e, dst.as_ref().join(file_name)))
                }
            }
        }
    }
    Ok(())
}
