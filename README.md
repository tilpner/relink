# relink

## What

relink is a simple tool that's supposed to reduce changes to your PATH variables.

## How

It is configured inside `<xdg-home-config>/relink`:

- `relink/in`: a symlink to a directory of input links, components of PATH
- `relink/out`: a simlink to the target directory, in which to link all elements

Just make sure that your PATH includes `out`.

### Example

```bash
$ mkdir in out a b
$ ln -s $PWD/in ~/.config/relink/in
$ ln -s $PWD/out ~/.config/relink/out
$ touch a/c a/d b/e b/f b/g
$ chmod +x a/d b/e b/g
$ ln -s $PWD/a $PWD/in/a
$ ln -s $PWD/b $PWD/in/b
$ relink
  b
    - g
    - e
  a
    - d
$ ls out
  d e g
```

## systemd unit

```bash
$ cp relink.service ~/.config/systemd/user/
$ systemctl daemon-reload
$ systemctl --user enable relink.service
$ systemctl --user start relink.service
```

## Why don't you just...

Please open an issue if you're aware of an easier alternative (!= nix) to this that I'm unaware of.
